close all
clear all
clc

A = xlsread('124_Accel_NN_Validation_Log_2015_06_11_11_34_35.csv');

 A(find (A(:,3)==0),:) = []  ;
 
 %Split raw data into different cells corresponding to inclination iterations
%A = arrayfun(@(x) Din(Din(:,1) == x, :), unique(Din(:,1)), 'uniformoutput', false);


l=1;
for j=1: max(A(:,1))
    for k=1: max(A(:,2))
        z= A(find(A(:,1)== j),:);
        Data (l,:) = mean ( z(find(z(:,2)== k),:),1);
        l=l+1;
    end
end

solution = zeros (size(Data,1),1);

for i=1: size(Data,1)
    
 solution(i) = myNeuralNetworkFunction_Bayesian([Data(i,11);Data(i,12);Data(i,13);Data(i,16);Data(i,17);Data(i,18);Data(i,24)]);   

end

Data(:,32) = solution;

error = abs(Data(:,31)- Data(:,32));

Data(:,33) = error;

max_absolute_error = max(error)

max_min_error= zeros( max(Data(:,1)),2);
for i=1: max(Data(:,1))
    max_min_error(i,2) =  ((max(Data(find(Data(:,1)==i),32))-min(Data(find(Data(:,1)==i),32)))/2);
     max_min_error(i,1)= mean ( Data(find(Data(:,1)==i),32)) ;
end

max_min_error