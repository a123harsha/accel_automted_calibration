close all
clear all
%clc

A = xlsread('121_Roll_Test_2015_06_22_16_33_23.csv');

 A(find (A(:,3)==0),:) = []  ;
 
 %Split raw data into different cells corresponding to inclination iterations
%A = arrayfun(@(x) Din(Din(:,1) == x, :), unique(Din(:,1)), 'uniformoutput', false);
raw_data = A(:,3:5);
load W1
load V1
load T1
cal_data = (W1*raw_data'+repmat(V1,1,size(raw_data,1)))';
cal_norm = sqrt(cal_data(:,1).^2 + cal_data(:,2).^2 + cal_data(:,3).^2);
cal_inc = (180/pi)*atan2(sqrt(cal_data(:,1).^2 + cal_data(:,2).^2), cal_data(:,3));


%A(:,23) = cal_inc ;
A(:,32) = abs(A(:,31) - A(:,23));
%A(:,32) = abs(A(:,31) - cal_inc);

l=1;
for j=1: max(A(:,1))
    for k=1: max(A(:,2))
        z= A(find(A(:,1)== j),:);
        Data (l,:) = mean ( z(find(z(:,2)== k),:),1);
        l=l+1;
    end
end



for j=1: max(A(:,1))
    
        z= A(find(A(:,1)== j),:);
        
        max_min_error(j) = (max(z(:,23))-min(z(:,23)))/2;
        Inc(j) = mean(z(:,31));
        Abs_error(j) = max(z(:,32));
    
end

Data_Analyzed = [Inc;Abs_error; max_min_error]
