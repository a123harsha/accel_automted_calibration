close all
clear all
clc

%NumberOfTempSweeps=4;
%NumberOfMagSweeps=42;
Error_Threshold=0.07;
sixty_parameters=1;

%Read Raw Data
D1=xlsread('124_Accel_Calibration_Log_2015_02_19_12_00_19.csv');


A2 =arrayfun(@(x) D1(D1(:,1) == x, :), unique(D1(:,1)), 'uniformoutput', false);
k = size(A2{1,1},1);
%Median filtering
for i=1 : max(max(D1(:,1)))
    D(i,1)=A2{i,1}(1,1);
    %D(i,3:9)=median(A2{i,1}(:,3:9),1);
    sorted= sort(A2{i,1}(:,2:10));
     D(i,2:10) = mean(  sorted(((k*0.3)+1):(k- (k*0.3)) ,:)  );
end

 %D(:,3)=D(:,3)*-1;
%D(:,4)=D(:,4)*-1;
% D(:,5)=D(:,5)*-1;
% D(:,9)=D(:,9)*-1;
%D(:,8)=D(:,8)*-1;

for j=1:size(D,1)
    theta=D(j,9);
    phi=D(j,10);
   D(j,11)= -cosd( theta) * cosd( phi);
   D(j,12)= cosd( theta) * sind( phi);
   D(j,13)= sind( theta);
end

%Split raw data into different cells corresponding to temperature iterations
%A = arrayfun(@(x) D(D(:,1) == x, :), unique(D(:,1)), 'uniformoutput', false);
A= cell(1,1);
A{1,1}=D;

W= cell(size(A));
V= cell(size(A));
T= cell(size(A));
error=cell(size(A));

%error_Avg= cell(size(A));
figure
for ii=1: size(A,1)
    
    first_time=0;
    
    while true
        
        if(first_time==1)
            %In every iteration, remove the worst outlier
            [a b]=find(error{ii,1}==max(max(error{ii,1})));
            aa=A{ii,1};
            aa(a,:)=[];
            A{ii,1}=aa;
            clear aa;
        end
        
        Data=zeros(size(A{ii,1}));
        Data=A{ii,1}(:,2:end);
        Gfx= Data(:,1);
        Gfy= Data(:,2);
        Gfz= Data(:,3);
        
%         Gfx= Gfx/750000;
%         Gfy= Gfy/750000;
%         Gfz= Gfz/750000;
        
        Xx=[ Gfx Gfy Gfz ones(size(Data,1),1) Gfx.^3];
        Xy=[ Gfx Gfy Gfz ones(size(Data,1),1) Gfy.^3];
        Xz=[ Gfx Gfy Gfz ones(size(Data,1),1) Gfz.^3];
        
        
        
        %X= X.* 0.0000002;
        %X(:,4) = ones(size(Data,1),1);
        
        
        Yx=Data(:,10);
        
        Yy=Data(:,11);
        
        Yz=Data(:,12);
        
        Bx= zeros(5,1);
        By= zeros(5,1);
        Bz= zeros(5,1);
        
        Bx= inv(Xx'*Xx) * Xx' * Yx;
        By= inv(Xy'*Xy) * Xy' * Yy;
        Bz= inv(Xz'*Xz) * Xz' * Yz;
        
        W{ii,1}= [Bx(1:3)' ; By(1:3)' ; Bz(1:3)'];
        V{ii,1}=[Bx(4);By(4);Bz(4)];
        T{ii,1}= [Bx(5) 0 0 ; 0 By(5) 0; 0 0 Bz(5)];
        CorrectedData=zeros(size(Data,1),3);
        
        for i=1:size(Data,1)
            CorrectedData(i,:) = W{ii,1} * Xx(i,1:3)' + V{ii,1} + T{ii,1}* [Xx(i,5);Xy(i,5);Xz(i,5)];
        end
        
        error{ii,1} =sqrt( (CorrectedData - Data(:,10:12)).^2);
        
        if(max(max(error{ii,1}))<=Error_Threshold)
            break
        end
        
        first_time=1;
        
    end
    %figure;
    if(size(A,1)~=1)
        subplot(2,2,ii);
    end
    grid on;
    hold on;
    axis square;
    plot3(Data(:,10),Data(:,11), Data(:,12), '*g');
    plot3(CorrectedData(:,1), CorrectedData(:,2), CorrectedData(:,3), '+b');
    xlabel('x');
    ylabel('y');
    zlabel('z');
    legend('Reference Data','Calibrated Data');
    title(['Temperature Iteration- ',num2str(ii),'; Total Samples- ',num2str(size(A{ii,1},1))]); %,'; Max error XYZ- ',num2str(max(error{ii,1}))]);
    error_Avg= mean (error{ii,1}',2)
    
end

Xc=CorrectedData(:,1);
Yc=CorrectedData(:,2);
Zc=CorrectedData(:,3);
Xoff=(max(max(Xc))+min(min(Xc)))/2
Yoff=(max(max(Yc))+min(min(Yc)))/2
Zoff=(max(max(Zc))+min(min(Zc)))/2

%V{1,1}=V{1,1}-[Xoff;Yoff;Zoff];
if(size(A,1)==1)
    
    %For room temperature
    T_FINAL1= [reshape(W{1,1}',9,1) zeros(9,1) zeros(9,1) zeros(9,1)];
    T_FINAL2= [reshape(V{1,1}',3,1) zeros(3,1) zeros(3,1) zeros(3,1)];
    T_FINAL3= [[T{1,1}(1,1);T{1,1}(2,2);T{1,1}(3,3)] zeros(3,1) zeros(3,1) zeros(3,1)];
    
    
else
    
    
    %For temperature compensation
    T1=mean(A{1,1}(:,9));
    T2=mean(A{2,1}(:,9));
    T3=mean(A{3,1}(:,9));
    T4=mean(A{4,1}(:,9));
    Temp=[T1;T2;T3;T4];
    
    
    Txx= inv([ones(4,1) Temp (Temp.^2) (Temp.^3)]) * [W{1,1}(1,1);W{2,1}(1,1);W{3,1}(1,1);W{4,1}(1,1)];
    Txy= inv([ones(4,1) Temp (Temp.^2) (Temp.^3)]) * [W{1,1}(1,2);W{2,1}(1,2);W{3,1}(1,2);W{4,1}(1,2)];
    Txz= inv([ones(4,1) Temp (Temp.^2) (Temp.^3)]) * [W{1,1}(1,3);W{2,1}(1,3);W{3,1}(1,3);W{4,1}(1,3)];
    Tyx= inv([ones(4,1) Temp (Temp.^2) (Temp.^3)]) * [W{1,1}(2,1);W{2,1}(2,1);W{3,1}(2,1);W{4,1}(2,1)];
    Tyy= inv([ones(4,1) Temp (Temp.^2) (Temp.^3)]) * [W{1,1}(2,2);W{2,1}(2,2);W{3,1}(2,2);W{4,1}(2,2)];
    Tyz= inv([ones(4,1) Temp (Temp.^2) (Temp.^3)]) * [W{1,1}(2,3);W{2,1}(2,3);W{3,1}(2,3);W{4,1}(2,3)];
    Tzx= inv([ones(4,1) Temp (Temp.^2) (Temp.^3)]) * [W{1,1}(3,1);W{2,1}(3,1);W{3,1}(3,1);W{4,1}(3,1)];
    Tzy= inv([ones(4,1) Temp (Temp.^2) (Temp.^3)]) * [W{1,1}(3,2);W{2,1}(3,2);W{3,1}(3,2);W{4,1}(3,2)];
    Tzz= inv([ones(4,1) Temp (Temp.^2) (Temp.^3)]) * [W{1,1}(3,3);W{2,1}(3,3);W{3,1}(3,3);W{4,1}(3,3)];
    
    Tx= inv([ones(4,1) Temp (Temp.^2) (Temp.^3)]) * [V{1,1}(1);V{2,1}(1);V{3,1}(1);V{4,1}(1)];
    Ty= inv([ones(4,1) Temp (Temp.^2) (Temp.^3)]) * [V{1,1}(2);V{2,1}(2);V{3,1}(2);V{4,1}(2)];
    Tz= inv([ones(4,1) Temp (Temp.^2) (Temp.^3)]) * [V{1,1}(3);V{2,1}(3);V{3,1}(3);V{4,1}(3)];
    
    Ttxx= inv([ones(4,1) Temp (Temp.^2) (Temp.^3)]) * [T{1,1}(1,1);T{2,1}(1,1);T{3,1}(1,1);T{4,1}(1,1)];
    Ttyy= inv([ones(4,1) Temp (Temp.^2) (Temp.^3)]) * [T{1,1}(2,2);T{2,1}(2,2);T{3,1}(2,2);T{4,1}(2,2)];
    Ttzz= inv([ones(4,1) Temp (Temp.^2) (Temp.^3)]) * [T{1,1}(3,3);T{2,1}(3,3);T{3,1}(3,3);T{4,1}(3,3)];
    
    T_FINAL1=[Txx'; Txy' ;Txz'; Tyx'; Tyy' ;Tyz'; Tzx' ;Tzy'; Tzz'];
    T_FINAL2=[Tx'; Ty'; Tz'];
    T_FINAL3=[Ttxx'; Ttyy'; Ttzz'];
    
end

T_Str='';
% for i=1:36
%     T_Str= strcat(T_Str,',',num2str(T_FINAL1(i)));
% end
% 
% for i=1:12
%     T_Str= strcat(T_Str,',',num2str(T_FINAL2(i)));
% end
% 
% 
% for i=1:12
%     T_Str= strcat(T_Str,',',num2str(T_FINAL3(i)));
% end

W2= W{1,1};
V2= V{1,1};
T2= T{1,1};
save('W2.mat','W2');
save('V2.mat','V2');
save('T2.mat','T2');

for i=1:9
    T_Str=strcat(T_Str,',',num2str(W2(i)));
end
%make it 60 parameter model for now

if(sixty_parameters==1)
for i=1:27
    T_Str=strcat(T_Str,',',num2str(0));
end
end

for i=1:3
    T_Str=strcat(T_Str,',',num2str(V{1,1}(i)));
end

%make it 60 parameter model for now
if(sixty_parameters==1)
for i=1:9
    T_Str=strcat(T_Str,',',num2str(0));
end
end


for i=1:3
    T_Str=strcat(T_Str,',',num2str(T{1,1}(i,i)));
end

%make it 60 parameter model for now
if(sixty_parameters==1)

for i=1:9
    T_Str=strcat(T_Str,',',num2str(0));
end
end

T_Str(1)=[];



figure
for ii=1: size(A,1)
    
    first_time=0;
    
    while true
        
        if(first_time==1)
            %In every iteration, remove the worst outlier
            [a b]=find(error{ii,1}==max(max(error{ii,1})));
            aa=A{ii,1};
            aa(a,:)=[];
            A{ii,1}=aa;
            clear aa;
        end
        
        Data=zeros(size(A{ii,1}));
        Data=A{ii,1}(:,2:end);
        Gfx= Data(:,4);
        Gfy= Data(:,5);
        Gfz= Data(:,6);
        
%         Gfx= Gfx/750000;
%         Gfy= Gfy/750000;
%         Gfz= Gfz/750000;
        
        Xx=[ Gfx Gfy Gfz ones(size(Data,1),1) Gfx.^3];
        Xy=[ Gfx Gfy Gfz ones(size(Data,1),1) Gfy.^3];
        Xz=[ Gfx Gfy Gfz ones(size(Data,1),1) Gfz.^3];
        
        
        
        %X= X.* 0.0000002;
        %X(:,4) = ones(size(Data,1),1);
        
        
        Yx=Data(:,10);
        
        Yy=Data(:,11);
        
        Yz=Data(:,12);
        
        Bx= zeros(5,1);
        By= zeros(5,1);
        Bz= zeros(5,1);
        
        Bx= inv(Xx'*Xx) * Xx' * Yx;
        By= inv(Xy'*Xy) * Xy' * Yy;
        Bz= inv(Xz'*Xz) * Xz' * Yz;
        
        W{ii,1}= [Bx(1:3)' ; By(1:3)' ; Bz(1:3)'];
        V{ii,1}=[Bx(4);By(4);Bz(4)];
        T{ii,1}= [Bx(5) 0 0 ; 0 By(5) 0; 0 0 Bz(5)];
        CorrectedData=zeros(size(Data,1),3);
        
        for i=1:size(Data,1)
            CorrectedData(i,:) = W{ii,1} * Xx(i,1:3)' + V{ii,1} + T{ii,1}* [Xx(i,5);Xy(i,5);Xz(i,5)];
        end
        
        error{ii,1} =sqrt( (CorrectedData - Data(:,10:12)).^2);
        
        if(max(max(error{ii,1}))<=Error_Threshold)
            break
        end
        
        first_time=1;
        
    end
    %figure;
    if(size(A,1)~=1)
        subplot(2,2,ii);
    end
    grid on;
    hold on;
    axis square;
    plot3(Data(:,10),Data(:,11), Data(:,12), '*g');
    plot3(CorrectedData(:,1), CorrectedData(:,2), CorrectedData(:,3), '+b');
    xlabel('x');
    ylabel('y');
    zlabel('z');
    legend('Reference Data','Calibrated Data');
    title(['Temperature Iteration- ',num2str(ii),'; Total Samples- ',num2str(size(A{ii,1},1))]); %,'; Max error XYZ- ',num2str(max(error{ii,1}))]);
    error_Avg= mean (error{ii,1}',2)
    
end

Xc=CorrectedData(:,1);
Yc=CorrectedData(:,2);
Zc=CorrectedData(:,3);
Xoff=(max(max(Xc))+min(min(Xc)))/2
Yoff=(max(max(Yc))+min(min(Yc)))/2
Zoff=(max(max(Zc))+min(min(Zc)))/2

%V{1,1}=V{1,1}-[Xoff;Yoff;Zoff];
if(size(A,1)==1)
    
    %For room temperature
    T_FINAL1= [reshape(W{1,1}',9,1) zeros(9,1) zeros(9,1) zeros(9,1)];
    T_FINAL2= [reshape(V{1,1}',3,1) zeros(3,1) zeros(3,1) zeros(3,1)];
    T_FINAL3= [[T{1,1}(1,1);T{1,1}(2,2);T{1,1}(3,3)] zeros(3,1) zeros(3,1) zeros(3,1)];
    
    
else
    
    
    %For temperature compensation
    T1=mean(A{1,1}(:,9));
    T2=mean(A{2,1}(:,9));
    T3=mean(A{3,1}(:,9));
    T4=mean(A{4,1}(:,9));
    Temp=[T1;T2;T3;T4];
    
    
    Txx= inv([ones(4,1) Temp (Temp.^2) (Temp.^3)]) * [W{1,1}(1,1);W{2,1}(1,1);W{3,1}(1,1);W{4,1}(1,1)];
    Txy= inv([ones(4,1) Temp (Temp.^2) (Temp.^3)]) * [W{1,1}(1,2);W{2,1}(1,2);W{3,1}(1,2);W{4,1}(1,2)];
    Txz= inv([ones(4,1) Temp (Temp.^2) (Temp.^3)]) * [W{1,1}(1,3);W{2,1}(1,3);W{3,1}(1,3);W{4,1}(1,3)];
    Tyx= inv([ones(4,1) Temp (Temp.^2) (Temp.^3)]) * [W{1,1}(2,1);W{2,1}(2,1);W{3,1}(2,1);W{4,1}(2,1)];
    Tyy= inv([ones(4,1) Temp (Temp.^2) (Temp.^3)]) * [W{1,1}(2,2);W{2,1}(2,2);W{3,1}(2,2);W{4,1}(2,2)];
    Tyz= inv([ones(4,1) Temp (Temp.^2) (Temp.^3)]) * [W{1,1}(2,3);W{2,1}(2,3);W{3,1}(2,3);W{4,1}(2,3)];
    Tzx= inv([ones(4,1) Temp (Temp.^2) (Temp.^3)]) * [W{1,1}(3,1);W{2,1}(3,1);W{3,1}(3,1);W{4,1}(3,1)];
    Tzy= inv([ones(4,1) Temp (Temp.^2) (Temp.^3)]) * [W{1,1}(3,2);W{2,1}(3,2);W{3,1}(3,2);W{4,1}(3,2)];
    Tzz= inv([ones(4,1) Temp (Temp.^2) (Temp.^3)]) * [W{1,1}(3,3);W{2,1}(3,3);W{3,1}(3,3);W{4,1}(3,3)];
    
    Tx= inv([ones(4,1) Temp (Temp.^2) (Temp.^3)]) * [V{1,1}(1);V{2,1}(1);V{3,1}(1);V{4,1}(1)];
    Ty= inv([ones(4,1) Temp (Temp.^2) (Temp.^3)]) * [V{1,1}(2);V{2,1}(2);V{3,1}(2);V{4,1}(2)];
    Tz= inv([ones(4,1) Temp (Temp.^2) (Temp.^3)]) * [V{1,1}(3);V{2,1}(3);V{3,1}(3);V{4,1}(3)];
    
    Ttxx= inv([ones(4,1) Temp (Temp.^2) (Temp.^3)]) * [T{1,1}(1,1);T{2,1}(1,1);T{3,1}(1,1);T{4,1}(1,1)];
    Ttyy= inv([ones(4,1) Temp (Temp.^2) (Temp.^3)]) * [T{1,1}(2,2);T{2,1}(2,2);T{3,1}(2,2);T{4,1}(2,2)];
    Ttzz= inv([ones(4,1) Temp (Temp.^2) (Temp.^3)]) * [T{1,1}(3,3);T{2,1}(3,3);T{3,1}(3,3);T{4,1}(3,3)];
    
    T_FINAL1=[Txx'; Txy' ;Txz'; Tyx'; Tyy' ;Tyz'; Tzx' ;Tzy'; Tzz'];
    T_FINAL2=[Tx'; Ty'; Tz'];
    T_FINAL3=[Ttxx'; Ttyy'; Ttzz'];
    
end

T_Str2='';
% for i=1:36
%     T_Str= strcat(T_Str,',',num2str(T_FINAL1(i)));
% end
% 
% for i=1:12
%     T_Str= strcat(T_Str,',',num2str(T_FINAL2(i)));
% end
% 
% 
% for i=1:12
%     T_Str= strcat(T_Str,',',num2str(T_FINAL3(i)));
% end
W2=W{1,1}';


for i=1:9
    T_Str2=strcat(T_Str2,',',num2str(W2(i)));
end
%make it 60 parameter model for now

if(sixty_parameters==1)
for i=1:27
    T_Str2=strcat(T_Str2,',',num2str(0));
end
end

for i=1:3
    T_Str2=strcat(T_Str2,',',num2str(V{1,1}(i)));
end

%make it 60 parameter model for now
if(sixty_parameters==1)
for i=1:9
    T_Str2=strcat(T_Str2,',',num2str(0));
end
end


for i=1:3
    T_Str2=strcat(T_Str2,',',num2str(T{1,1}(i,i)));
end

%make it 60 parameter model for now
if(sixty_parameters==1)

for i=1:9
    T_Str2=strcat(T_Str2,',',num2str(0));
end
end

T_Str2(1)=[];




