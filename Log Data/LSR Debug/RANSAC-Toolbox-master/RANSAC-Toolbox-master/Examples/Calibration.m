% NAME:
% test_RANSAC_circles.m
%
% DESC:
% test to estimate the parameters of a circle. This examples illustrates
% also how to pass parameters to the estimation and error functions

close all
clear all
 clc
%% Below is the error threshold (in mg) to calibration error
Error_Threshold= 7.3;
%%  Read Raw Data
Din=xlsread('124_Accel_Calibration_Log_2015_05_29_12_13_08.csv');
Din(find (Din(:,3)==0),:) = []  ; %Remove all zeros
%% generate reference components
for j=1:size(Din,1)
    theta=Din(j,11);
    phi=Din(j,12);
    Din(j,13)= -cosd( theta) * cosd( phi) * 0.9984;
    Din(j,14)= cosd( theta) * sind( phi) * 0.9984;
    Din(j,15)= sind( theta) * 0.9984;
end

%% median filtering
D_filtered = zeros (max(Din(:,2)),15);
for i=1 : max(Din(:,2))
    D_temp = Din(find (Din(:,2)==i),:);
    D_filtered(i,1) = D_temp(1,1);
    D_filtered(i,2) = D_temp(1,2);
    offset = round(0.1*size(D_temp,1));
    data_norm = zeros(1,size(D_temp,1));
    for j=1:size(D_temp,1)
        data_norm(j) = norm(D_temp(j,3:5));
    end
    [dummy, ind] = sort(data_norm);
    filtered_data = D_temp(ind(1+offset:end-offset),3:15);
    D_filtered(i,3:15) = mean( filtered_data );
end
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Parameters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% radii of the circles
radii = [0.1, 0.2,904];
% center of the circles
xc = [0.5, 0.4, 0];
yc = [0.8, 0.5, 0];
zc = [0.8, 0.5, 0];
% number of points on the circles
n_p = [20 30 40];
% total number of outliers
n_o = 5;
% noise
sigma = 2;
% the circle to identify
n_c_star = 3;

% set RANSAC options
options.epsilon = 1e-6;
options.P_inlier = 1-1e-4;
options.sigma = sigma;
options.est_fun = @estimate_circle;
options.man_fun = @error_circle;
options.mode = 'MSAC';
options.Ps = [];
options.notify_iters = [];
options.min_iters = 100;
options.max_iters = 10000;
options.fix_seed = false;
options.reestimate = true;
options.stabilize = false;

% here we set theradius of the circle that we want to detect
options.parameters.radius = radii(n_c_star);

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Data Generation
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% make it pseudo-random
% seed = 34545;
% rand('twister', seed);
% randn('state', seed);

% X = rand(2, n_o);
% 
% for n_c = 1:numel(radii)
%     phi = 2*pi*rand(1, n_p(n_c));    
%     x = xc(n_c)+radii(n_c)*cos(phi);
%     y = yc(n_c)+radii(n_c)*sin(phi);
%     C = [x;y] + sigma*randn(2,n_p(n_c));
%     X = [X C];
% end

% X = [D_filtered(:,13)';D_filtered(:,14)';D_filtered(:,15)'];
X = [D_filtered(:,3)';D_filtered(:,4)';D_filtered(:,5)'];
% scramble (just in case...)
[dummy, ind] = sort(rand(1, size(X,2)));
X = X(:, ind);

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% RANSAC
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% run RANSAC
[results, options] = RANSAC(X, options);

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Results Visualization
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
figure;
hold on
plot3(X(1,:), X(2, :),X(3, :), '+r');
plot3(X(1,results.CS), X(2, results.CS), X(3, results.CS), 'sg');
xlabel('x');
ylabel('y');
zlabel('z');
axis equal tight
grid on

phi = linspace(-pi,pi,128);

% x_c = results.Theta(1) + radii(n_c_star)*cos(phi);
% y_c = results.Theta(2) + radii(n_c_star)*sin(phi);
% plot([x_c x_c(1)], [y_c y_c(1)], 'g', 'LineWidth', 2)
% 
% x_c = xc(n_c_star) + radii(n_c_star)*cos(phi);
% y_c = yc(n_c_star) + radii(n_c_star)*sin(phi);
% plot([x_c x_c(1)], [y_c y_c(1)], '--b', 'LineWidth', 1)

title('Green is RANSAC estimate, blue is ground truth')
