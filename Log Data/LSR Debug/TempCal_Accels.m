close all
clear all
clc

%NumberOfTempSweeps=4;
%NumberOfMagSweeps=42;

%Below is the error threshold to calibration error
Error_Threshold=0.008;

       
%below is the error threshold to inclination error
%Error_Threshold=0.1;


%Read Raw Data
Din=xlsread('117_Accel_Calibration_Log_2015_06_19_11_16_46.csv');


%A2 =arrayfun(@(x) D1(D1(:,1) == x, :), unique(D1(:,1)), 'uniformoutput', false);
%k = size(A2{1,1},1);
%Median filtering
% for i=1 : max(max(D1(:,1)))
%     D(i,1)=A2{i,1}(1,1);
%     %D(i,3:9)=median(A2{i,1}(:,3:9),1);
%     sorted= sort(A2{i,1}(:,2:10));
%      D(i,2:10) = mean(  sorted(((k*0.3)+1):(k- (k*0.3)) ,:)  );
% end

 %D(:,3)=D(:,3)*-1;
%D(:,4)=D(:,4)*-1;
% D(:,5)=D(:,5)*-1;
% D(:,9)=D(:,9)*-1;
%D(:,8)=D(:,8)*-1;

for j=1:size(Din,1)
    theta=Din(j,11);
    phi=Din(j,12);
   Din(j,13)= -cosd( theta) * cosd( phi) * 0.9984;
   Din(j,14)= cosd( theta) * sind( phi) * 0.9984;
   Din(j,15)= sind( theta) * 0.9984;
end





%Split raw data into different cells corresponding to temperature iterations
A = arrayfun(@(x) Din(Din(:,1) == x, :), unique(Din(:,1)), 'uniformoutput', false);

  for jj=1:size(A,1)
    Temp(jj)=mean(A{jj,1}(:,10));
    end

W= cell(size(A));
V= cell(size(A));
T= cell(size(A));
error=cell(size(A));

%error_Avg= cell(size(A));
figure
for ii=1: size(A,1)
    
    D2=[];
    D=[];
    A2=[];
    D= A{ii,1};
    A2 =arrayfun(@(x) D(D(:,2) == x, :), unique(D(:,2)), 'uniformoutput', false);
    
   
for i=1 : max(max(D(:,2)))    
    A2{i,1}(find (A2{i,1}(:,3)==0),:) = []  ;
    offset = round(0.1*size(A2{i,1},1));
    D2(i,1)=A2{i,1}(1,1);
    D2(i,2)=A2{i,1}(1,2);
    %     D2(i,3:13)=median(A2{i,1}(:,3:13),1); median filtering
    %      sorted= sort(A2{i,1}(:,3:15));
    data_norm = zeros(1,size(A2{i,1},1));
    for j=1:size(A2{i,1},1)
        data_norm(j) = norm(A2{i,1}(j,3:5));
    end
    [dummy, ind] = sort(data_norm);
    filtered_data = A2{i,1}(ind(1+offset:end-offset),3:15);
    D2(i,3:15) = mean( filtered_data );
end
    
A{ii,1}=[];
A{ii,1}=D2;
    first_time=0;
    
    while true
        
        if(first_time==1)
            %In every iteration, remove the worst outlier
            [a b]=find(error{ii,1}==max(max(error{ii,1})));
            aa=A{ii,1};
            aa(a,:)=[];
            A{ii,1}=aa;
            clear aa;
        end
        
        Data=zeros(size(A{ii,1}));
        Data=A{ii,1}(:,2:end);
        Gfx= Data(:,2);
        Gfy= Data(:,3);
        Gfz= Data(:,4);
        
%         Gfx= Gfx/750000;
%         Gfy= Gfy/750000;
%         Gfz= Gfz/750000;
        
        Xx=[ Gfx Gfy Gfz ones(size(Data,1),1) Gfx.^3];
        Xy=[ Gfx Gfy Gfz ones(size(Data,1),1) Gfy.^3];
        Xz=[ Gfx Gfy Gfz ones(size(Data,1),1) Gfz.^3];
        
        
        
        %X= X.* 0.0000002;
        %X(:,4) = ones(size(Data,1),1);
        
        
        Yx=Data(:,12);
        
        Yy=Data(:,13);
        
        Yz=Data(:,14);
        
        Bx= zeros(5,1);
        By= zeros(5,1);
        Bz= zeros(5,1);
        
        Bx= inv(Xx'*Xx) * Xx' * Yx;
        By= inv(Xy'*Xy) * Xy' * Yy;
        Bz= inv(Xz'*Xz) * Xz' * Yz;
        
        W{ii,1}= [Bx(1:3)' ; By(1:3)' ; Bz(1:3)'];
        V{ii,1}=[Bx(4);By(4);Bz(4)];
        T{ii,1}= [Bx(5) 0 0 ; 0 By(5) 0; 0 0 Bz(5)];
        CorrectedData=zeros(size(Data,1),3);
        Inclination=zeros(size(Data,1),1);
        for i=1:size(Data,1)
            CorrectedData(i,:) = W{ii,1} * Xx(i,1:3)' + V{ii,1} + T{ii,1}* [Xx(i,5);Xy(i,5);Xz(i,5)];
            Ax1=CorrectedData(i,1);
            Ay1=CorrectedData(i,2);
            Az1=CorrectedData(i,3);

            Grav1=sqrt(Ax1^2+Ay1^2+Az1^2);
            Inc1= atand(sqrt(Ax1^2+Ay1^2)/Az1);

            if(Inc1<0)
            Inc1 =  180 + Inc1;
            end
            Inclination(i) = Inc1;
            
            A{ii,1}(:,16) =  90 - A{ii,1}(:,11);
            A{ii,1}(:,17) = Inclination;
            A{ii,1}(:,18) = A{ii,1}(:,16) - A{ii,1}(:,17);
           % A{ii,1}(:,16:18)
            
        end
        
        %Below is the error threshold to calibration error
        error{ii,1} =sqrt( (CorrectedData - [Yx Yy Yz]).^2);
        
        %below is the error threshold to inclination error
        %error{ii,1} =A{ii,1}(:,18);
        max(max(error{ii,1}))
        if(max(max(error{ii,1}))<=Error_Threshold)
            break
        end
        
        first_time=1;
        
    end
    %figure;
    if(size(A,1)~=1)
        subplot(ceil(size(A,1)/2),2,ii);
    end
    grid on;
    hold on;
    axis square;
    plot3(Yx,Yy,Yz, '*g');
    plot3(CorrectedData(:,1), CorrectedData(:,2), CorrectedData(:,3), '+b');
    xlabel('x');
    ylabel('y');
    zlabel('z');
    title(['Temperature Iteration- ',num2str(Temp(ii)),'; Total Samples- ',num2str(size(A{ii,1},1))]); %,'; Max error XYZ- ',num2str(max(error{ii,1}))]);
    error_Avg= mean (error{ii,1}',2)
    
end






% W3= W{1,1};
% V3= V{1,1};
% T3= T{1,1};
% save('W3.mat','W3');
% save('V3.mat','V3');
% save('T3.mat','T3');

if(size(A,1)==1)
    
    
W1= W{1,1};
V1= V{1,1};
T1= T{1,1};

save('W1.mat','W1');
save('V1.mat','V1');
save('T1.mat','T1');

    %For room temperature
    T_FINAL1= [reshape(W{1,1}',9,1) zeros(9,1) zeros(9,1) zeros(9,1)];
    T_FINAL2= [reshape(V{1,1}',3,1) zeros(3,1) zeros(3,1) zeros(3,1)];
    T_FINAL3= [[T{1,1}(1,1);T{1,1}(2,2);T{1,1}(3,3)] zeros(3,1) zeros(3,1) zeros(3,1)];
    
    
        
    
else
    
     W1= W{1,1};
V1= V{1,1};
T1= T{1,1};

save('W1.mat','W1');
save('V1.mat','V1');
save('T1.mat','T1');
    %For temperature compensation
    

%     T2=mean(A{2,1}(:,9));
%     T3=mean(A{3,1}(:,9));
%     T4=mean(A{4,1}(:,9));
%     T5=mean(A{5,1}(:,9));
%     Temp=[T1;T2;T3;T4;T5];
    
Temp= Temp';
    XT= [ones(size(A,1),1) Temp (Temp.^2) (Temp.^3)];
    
    
    for kk=1:size(A,1)
        
    Xwxx(kk)= W{kk,1}(1,1);
    Xwxy(kk)= W{kk,1}(1,2);
    Xwxz(kk)= W{kk,1}(1,3);
    Xwyx(kk)= W{kk,1}(2,1);
    Xwyy(kk)= W{kk,1}(2,2);
    Xwyz(kk)= W{kk,1}(2,3);
    Xwzx(kk)= W{kk,1}(3,1);
    Xwzy(kk)= W{kk,1}(3,2);
    Xwzz(kk)= W{kk,1}(3,3);
    
    Xvx(kk)= V{kk,1}(1);
    Xvy(kk)= V{kk,1}(2);
    Xvz(kk)= V{kk,1}(3);
    
    Xtxx(kk)= T{kk,1}(1,1);
    Xtyy(kk)= T{kk,1}(2,2);
    Xtzz(kk)= T{kk,1}(3,3);
    
    end
    
    
    Twxx= inv(XT'*XT)* XT' * Xwxx';
    Twxy= inv(XT'*XT)* XT' * Xwxy';
    Twxz= inv(XT'*XT)* XT' * Xwxz';
    Twyx= inv(XT'*XT)* XT' * Xwyx';
    Twyy= inv(XT'*XT)* XT' * Xwyy';
    Twyz= inv(XT'*XT)* XT' * Xwyz';
    Twzx= inv(XT'*XT)* XT' * Xwzx';
    Twzy= inv(XT'*XT)* XT' * Xwzy';
    Twzz= inv(XT'*XT)* XT' * Xwzz';
    
    Tvx = inv(XT'*XT)* XT' * Xvx';
    Tvy = inv(XT'*XT)* XT' * Xvy';
    Tvz = inv(XT'*XT)* XT' * Xvz';
    
    Ttxx= inv(XT'*XT)* XT' * Xtxx';
    Ttyy= inv(XT'*XT)* XT' * Xtyy';
    Ttzz= inv(XT'*XT)* XT' * Xtzz';
    
    T_FINAL1=[Twxx'; Twxy' ;Twxz'; Twyx'; Twyy' ;Twyz'; Twzx' ;Twzy'; Twzz'];
    T_FINAL2=[Tvx'; Tvy'; Tvz'];
    T_FINAL3=[Ttxx'; Ttyy'; Ttzz'];
    
end

T_Str='';
for i=1:36
    T_Str= strcat(T_Str,',',num2str(T_FINAL1(i)));
end

for i=1:12
    T_Str= strcat(T_Str,',',num2str(T_FINAL2(i)));
end


for i=1:12
    T_Str= strcat(T_Str,',',num2str(T_FINAL3(i)));
end

T_Str(1)=[];


if(size(A,1)>1)

    W_TempCal{1,1}=Twxx;
    W_TempCal{2,1}=Twyx;
    W_TempCal{3,1}=Twzx;
    W_TempCal{4,1}=Twxy;
    W_TempCal{5,1}=Twyy;
    W_TempCal{6,1}=Twzy;
    W_TempCal{7,1}=Twxz;
    W_TempCal{8,1}=Twyz;
    W_TempCal{9,1}=Twzz;
    
    V_TempCal{1,1}= Tvx;
    V_TempCal{2,1}= Tvy;
    V_TempCal{3,1}= Tvz;
    
    T_TempCal{1,1} = Ttxx;
    T_TempCal{2,1} = zeros(1,4);
    T_TempCal{3,1} = zeros(1,4);
    T_TempCal{4,1} = zeros(1,4);
    T_TempCal{5,1} = Ttyy;
    T_TempCal{6,1} = zeros(1,4);
    T_TempCal{7,1} = zeros(1,4);
    T_TempCal{8,1} = zeros(1,4);
    T_TempCal{9,1} = Ttzz;
    
    
    
    
    
    
    
    
    
    
    figure
    for i=1:9
        W_plot=[];
        W_fit =[];
        for j=1: length(W)
            W_plot(j)= W{j,1}(i);
            W_fit(j) = W_TempCal{i,1}(1)+ W_TempCal{i,1}(2)*Temp(j)+ W_TempCal{i,1}(3)*(Temp(j))^2+W_TempCal{i,1}(4)*(Temp(j))^3;
        end
        
        subplot(3,3,i);
        plot (Temp,W_plot,'r*')
        hold on
        plot (Temp,W_fit,'b*')
        %legend('Raw', 'Cal fit');
        title(['W(',num2str(i),')']);
    end
    
    
    
    figure
    for i=1:3
        V_plot=[];
        V_fit=[];
        for j=1: length(V)
            V_plot(j)= V{j,1}(i);
            V_fit(j) = V_TempCal{i,1}(1)+ V_TempCal{i,1}(2)*Temp(j)+ V_TempCal{i,1}(3)*(Temp(j))^2+V_TempCal{i,1}(4)*(Temp(j))^3;
        end
        
        subplot(3,1,i);
        plot (Temp,V_plot,'r*')
        hold on
        plot (Temp,V_fit,'b*')
        title(['V(',num2str(i),')']);
    end
    
    figure
    for i=1:9
        T_plot=[];
        T_fit=[];
        for j=1: length(T)
            T_plot(j)= T{j,1}(i);
            T_fit(j) = T_TempCal{i,1}(1)+ T_TempCal{i,1}(2)*Temp(j)+ T_TempCal{i,1}(3)*(Temp(j))^2+T_TempCal{i,1}(4)*(Temp(j))^3;
        end
        
        subplot(3,3,i);
        plot (Temp,T_plot,'r*')
        hold on
        plot (Temp,T_fit,'b*')
        title(['T(',num2str(i),')']);
    end

end


%For accel2


A = arrayfun(@(x) Din(Din(:,1) == x, :), unique(Din(:,1)), 'uniformoutput', false);

W= cell(size(A));
V= cell(size(A));
T= cell(size(A));
error=cell(size(A));

figure
for ii=1: size(A,1)
    
    D2=[];
    D=[];
    A2=[];
    D= A{ii,1};
    A2 =arrayfun(@(x) D(D(:,2) == x, :), unique(D(:,2)), 'uniformoutput', false);

   
for i=1 : max(max(D(:,2)))
    
     A2{i,1}(find (A2{i,1}(:,3)==0),:) = []  ;
    k = size(A2{i,1},1);
     D2(i,1)=A2{i,1}(1,1);
     D2(i,2)=A2{i,1}(1,2);
%     D2(i,3:13)=median(A2{i,1}(:,3:13),1); median filtering
     sorted= sort(A2{i,1}(:,3:15));
     D2(i,3:15) = mean(  sorted(((k*0.2)+1):(k- (k*0.2)) ,:)  );
end
    
A{ii,1}=[];
A{ii,1}=D2;
    first_time=0;
    
    while true
        
        if(first_time==1)
            %In every iteration, remove the worst outlier
            [a b]=find(error{ii,1}==max(max(error{ii,1})));
            aa=A{ii,1};
            aa(a,:)=[];
            A{ii,1}=aa;
            clear aa;
        end
        
        Data=zeros(size(A{ii,1}));
        Data=A{ii,1}(:,2:end);
        Gfx= Data(:,5);
        Gfy= Data(:,6);
        Gfz= Data(:,7);
        
%         Gfx= Gfx/750000;
%         Gfy= Gfy/750000;
%         Gfz= Gfz/750000;
        
        Xx=[ Gfx Gfy Gfz ones(size(Data,1),1) Gfx.^3];
        Xy=[ Gfx Gfy Gfz ones(size(Data,1),1) Gfy.^3];
        Xz=[ Gfx Gfy Gfz ones(size(Data,1),1) Gfz.^3];
        
        
        
        %X= X.* 0.0000002;
        %X(:,4) = ones(size(Data,1),1);
        
        
        Yx=Data(:,12);
        
        Yy=Data(:,13);
        
        Yz=Data(:,14);
        
        Bx= zeros(5,1);
        By= zeros(5,1);
        Bz= zeros(5,1);
        
        Bx= inv(Xx'*Xx) * Xx' * Yx;
        By= inv(Xy'*Xy) * Xy' * Yy;
        Bz= inv(Xz'*Xz) * Xz' * Yz;
        
        W{ii,1}= [Bx(1:3)' ; By(1:3)' ; Bz(1:3)'];
        V{ii,1}=[Bx(4);By(4);Bz(4)];
        T{ii,1}= [Bx(5) 0 0 ; 0 By(5) 0; 0 0 Bz(5)];
        CorrectedData2=zeros(size(Data,1),3);
        
        for i=1:size(Data,1)
            CorrectedData2(i,:) = W{ii,1} * Xx(i,1:3)' + V{ii,1} + T{ii,1}* [Xx(i,5);Xy(i,5);Xz(i,5)];
        end
        
        error{ii,1} =sqrt( (CorrectedData2 - [Yx Yy Yz]).^2);
        
        if(max(max(error{ii,1}))<=Error_Threshold)
            break
        end
        
        first_time=1;
        
    end
    %figure;
    if(size(A,1)~=1)
        subplot(ceil(size(A,1)/2),2,ii);
    end
    grid on;
    hold on;
    axis square;
    plot3(Yx,Yy,Yz, '*g');
    plot3(CorrectedData2(:,1), CorrectedData2(:,2), CorrectedData2(:,3), '+b');
    xlabel('x');
    ylabel('y');
    zlabel('z');
    title(['Temperature Iteration- ',num2str(Temp(ii)),'; Total Samples- ',num2str(size(A{ii,1},1))]); %,'; Max error XYZ- ',num2str(max(error{ii,1}))]);
    error_Avg= mean (error{ii,1}',2)
    
end






% W3= W{1,1};
% V3= V{1,1};
% T3= T{1,1};
% save('W3.mat','W3');
% save('V3.mat','V3');
% save('T3.mat','T3');

if(size(A,1)==1)
    
    
    W2= W{1,1};
V2= V{1,1};
T2= T{1,1};

save('W2.mat','W2');
save('V2.mat','V2');
save('T2.mat','T2');

    %For room temperature
    T_FINAL1= [reshape(W{1,1}',9,1) zeros(9,1) zeros(9,1) zeros(9,1)];
    T_FINAL2= [reshape(V{1,1}',3,1) zeros(3,1) zeros(3,1) zeros(3,1)];
    T_FINAL3= [[T{1,1}(1,1);T{1,1}(2,2);T{1,1}(3,3)] zeros(3,1) zeros(3,1) zeros(3,1)];
    
    
else
    
    W2= W{1,1};
V2= V{1,1};
T2= T{1,1};
save('W2.mat','W2');
save('V2.mat','V2');
save('T2.mat','T2');
    %For temperature compensation
    
  
%     T2=mean(A{2,1}(:,9));
%     T3=mean(A{3,1}(:,9));
%     T4=mean(A{4,1}(:,9));
%     T5=mean(A{5,1}(:,9));
%     Temp=[T1;T2;T3;T4;T5];
    

    XT= [ones(size(A,1),1) Temp (Temp.^2) (Temp.^3)];
    
    
    for kk=1:size(A,1)
        
    Xwxx(kk)= W{kk,1}(1,1);
    Xwxy(kk)= W{kk,1}(1,2);
    Xwxz(kk)= W{kk,1}(1,3);
    Xwyx(kk)= W{kk,1}(2,1);
    Xwyy(kk)= W{kk,1}(2,2);
    Xwyz(kk)= W{kk,1}(2,3);
    Xwzx(kk)= W{kk,1}(3,1);
    Xwzy(kk)= W{kk,1}(3,2);
    Xwzz(kk)= W{kk,1}(3,3);
    
    Xvx(kk)= V{kk,1}(1);
    Xvy(kk)= V{kk,1}(2);
    Xvz(kk)= V{kk,1}(3);
    
    Xtxx(kk)= T{kk,1}(1,1);
    Xtyy(kk)= T{kk,1}(2,2);
    Xtzz(kk)= T{kk,1}(3,3);
    
    end
    
    
    Twxx= inv(XT'*XT)* XT' * Xwxx';
    Twxy= inv(XT'*XT)* XT' * Xwxy';
    Twxz= inv(XT'*XT)* XT' * Xwxz';
    Twyx= inv(XT'*XT)* XT' * Xwyx';
    Twyy= inv(XT'*XT)* XT' * Xwyy';
    Twyz= inv(XT'*XT)* XT' * Xwyz';
    Twzx= inv(XT'*XT)* XT' * Xwzx';
    Twzy= inv(XT'*XT)* XT' * Xwzy';
    Twzz= inv(XT'*XT)* XT' * Xwzz';
    
    Tvx = inv(XT'*XT)* XT' * Xvx';
    Tvy = inv(XT'*XT)* XT' * Xvy';
    Tvz = inv(XT'*XT)* XT' * Xvz';
    
    Ttxx= inv(XT'*XT)* XT' * Xtxx';
    Ttyy= inv(XT'*XT)* XT' * Xtyy';
    Ttzz= inv(XT'*XT)* XT' * Xtzz';
    
    T_FINAL1=[Twxx'; Twxy' ;Twxz'; Twyx'; Twyy' ;Twyz'; Twzx' ;Twzy'; Twzz'];
    T_FINAL2=[Tvx'; Tvy'; Tvz'];
    T_FINAL3=[Ttxx'; Ttyy'; Ttzz'];
    
end

T_Str2='';
for i=1:36
    T_Str2= strcat(T_Str2,',',num2str(T_FINAL1(i)));
end

for i=1:12
    T_Str2= strcat(T_Str2,',',num2str(T_FINAL2(i)));
end


for i=1:12
    T_Str2= strcat(T_Str2,',',num2str(T_FINAL3(i)));
end

T_Str2(1)=[];


if(size(A,1)>1)

    W_TempCal{1,1}=Twxx;
    W_TempCal{2,1}=Twyx;
    W_TempCal{3,1}=Twzx;
    W_TempCal{4,1}=Twxy;
    W_TempCal{5,1}=Twyy;
    W_TempCal{6,1}=Twzy;
    W_TempCal{7,1}=Twxz;
    W_TempCal{8,1}=Twyz;
    W_TempCal{9,1}=Twzz;
    
    V_TempCal{1,1}= Tvx;
    V_TempCal{2,1}= Tvy;
    V_TempCal{3,1}= Tvz;
    
    T_TempCal{1,1} = Ttxx;
    T_TempCal{2,1} = zeros(1,4);
    T_TempCal{3,1} = zeros(1,4);
    T_TempCal{4,1} = zeros(1,4);
    T_TempCal{5,1} = Ttyy;
    T_TempCal{6,1} = zeros(1,4);
    T_TempCal{7,1} = zeros(1,4);
    T_TempCal{8,1} = zeros(1,4);
    T_TempCal{9,1} = Ttzz;
    
    
    
    
    
    
    
    
    
    
    figure
    for i=1:9
        W_plot=[];
        W_fit =[];
        for j=1: length(W)
            W_plot(j)= W{j,1}(i);
            W_fit(j) = W_TempCal{i,1}(1)+ W_TempCal{i,1}(2)*Temp(j)+ W_TempCal{i,1}(3)*(Temp(j))^2+W_TempCal{i,1}(4)*(Temp(j))^3;
        end
        
        subplot(3,3,i);
        plot (Temp,W_plot,'r*')
        hold on
        plot (Temp,W_fit,'b*')
        %legend('Raw', 'Cal fit');
        title(['W(',num2str(i),')']);
    end
    
    
    
    figure
    for i=1:3
        V_plot=[];
        V_fit=[];
        for j=1: length(V)
            V_plot(j)= V{j,1}(i);
            V_fit(j) = V_TempCal{i,1}(1)+ V_TempCal{i,1}(2)*Temp(j)+ V_TempCal{i,1}(3)*(Temp(j))^2+V_TempCal{i,1}(4)*(Temp(j))^3;
        end
        
        subplot(3,1,i);
        plot (Temp,V_plot,'r*')
        hold on
        plot (Temp,V_fit,'b*')
        title(['V(',num2str(i),')']);
    end
    
    figure
    for i=1:9
        T_plot=[];
        T_fit=[];
        for j=1: length(T)
            T_plot(j)= T{j,1}(i);
            T_fit(j) = T_TempCal{i,1}(1)+ T_TempCal{i,1}(2)*Temp(j)+ T_TempCal{i,1}(3)*(Temp(j))^2+T_TempCal{i,1}(4)*(Temp(j))^3;
        end
        
        subplot(3,3,i);
        plot (Temp,T_plot,'r*')
        hold on
        plot (Temp,T_fit,'b*')
        title(['T(',num2str(i),')']);
    end

end