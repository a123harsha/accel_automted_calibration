close all
clear all
clc

%% Below is the error threshold (in mg) to calibration error
Error_Threshold= 7.3;
%%  Read Raw Data
Din=xlsread('124_Accel_Calibration_Log_2015_05_29_12_13_08.csv');
Din(find (Din(:,3)==0),:) = []  ; %Remove all zeros
%% generate reference components
for j=1:size(Din,1)
    theta=Din(j,11);
    phi=Din(j,12);
    Din(j,13)= -cosd( theta) * cosd( phi) * 0.9984;
    Din(j,14)= cosd( theta) * sind( phi) * 0.9984;
    Din(j,15)= sind( theta) * 0.9984;
end

%% median filtering
D_filtered = zeros (max(Din(:,2)),15);
for i=1 : max(Din(:,2))
    D_temp = Din(find (Din(:,2)==i),:);
    D_filtered(i,1) = D_temp(1,1);
    D_filtered(i,2) = D_temp(1,2);
    offset = round(0.1*size(D_temp,1));
    data_norm = zeros(1,size(D_temp,1));
    for j=1:size(D_temp,1)
        data_norm(j) = norm(D_temp(j,3:5));
    end
    [dummy, ind] = sort(data_norm);
    filtered_data = D_temp(ind(1+offset:end-offset),3:15);
    D_filtered(i,3:15) = mean( filtered_data );
end
%% LSR
Data= D_filtered;

%Raw Values
Gfx= Data(:,3);
Gfy= Data(:,4);
Gfz= Data(:,5);
% Xx=[ exp(1/Gfx)' exp(1/Gfy)' exp(1/Gfz)' ones(size(Data,1),1) Gfx.^3];
% Xy=[ exp(1/Gfx)' exp(1/Gfy)' exp(1/Gfz)' ones(size(Data,1),1) Gfy.^3];
% Xz=[ exp(1/Gfx)' exp(1/Gfy)' exp(1/Gfz)' ones(size(Data,1),1) Gfz.^3];
Xx=[ Gfx Gfy Gfz ones(size(Data,1),1) Gfx.^3];
Xy=[ Gfx Gfy Gfz ones(size(Data,1),1) Gfy.^3];
Xz=[ Gfx Gfy Gfz ones(size(Data,1),1) Gfz.^3];
%References
Yx=Data(:,13);
Yy=Data(:,14);
Yz=Data(:,15);
weights= 50* ones(size(Data,1),1);
previous_max_error =0;
while true
    %solution
    Bx= zeros(5,1);
    By= zeros(5,1);
    Bz= zeros(5,1);
    costfunx =@(w) (Yx-Xx*w') ;
    costfuny =@(w) (Yy-Xy*w') ;
    costfunz =@(w) (Yz-Xz*w') ;
    upperbound = [100 100 100 100 100];
    lowerbound = [-100 -100 -100 -100 -100];
    options = optimset('Display','iter','FunValCheck','on','MaxFunEvals',Inf, ...
        'MaxIter',2000,'TolFun',1e-20,'TolX',1e-20);
    seed = [0 0 0 0 0];
    Bx = lsqnonlin(costfunx,seed,[],[],options); %lsqnonlin(costfunx,seed,lowerbound,upperbound,options);
    By = lsqnonlin(costfuny,seed,[],[],options);
    Bz = lsqnonlin(costfunz,seed,[],[],options);
    %     Bx= lscov(Xx,Yx,weights);
    %     By= lscov(Xy,Yy,weights);
    %     Bz= lscov(Xz,Yz,weights);
    Bx = Bx';
    By = By';
    Bz = Bz';
    W= [Bx(1:3)' ; By(1:3)' ; Bz(1:3)'];
    V=[Bx(4);By(4);Bz(4)];
    T= [Bx(5) 0 0 ; 0 By(5) 0; 0 0 Bz(5)];
    %correct the raw readings
    for i=1:size(Data,1)
        CorrectedData(i,:) = W * Xx(i,1:3)' + V + T* [Xx(i,5);Xy(i,5);Xz(i,5)];
    end
    %calculate errors
    error =abs( CorrectedData - [Yx Yy Yz]);
    error(:,4) = sqrt(error(:,1).^2+error(:,2).^2+error(:,3).^2);
    max_error = max(error(:,4))*1000;
    [Errors_descending error_indices_descending]= sort(error(:,4),'descend');
    max_error = Errors_descending(1)*1000;
    max_error_index = error_indices_descending(1);
    disp(['Max error: ',num2str(max_error),' mg at the point [X Y Z] = ['...
        ,num2str(Data(max_error_index,13:15)), '] with index: ', num2str(max_error_index)...
        ,' Avg error: ',num2str(mean(error(:,4))*1000)]);
    %check if error is within the limits and stop
    break
    if max_error < Error_Threshold | abs(previous_max_error - max_error) < 0.0000001
        break
    else
       weights(error_indices_descending(1:3)) = weights(error_indices_descending(1:3))+1;
    end
    
    previous_max_error = max_error;
end
