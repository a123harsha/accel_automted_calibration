% test ellipsoid fit
close all
clear all
clc

% create the test data:
% radii
% a = 8;
% b = 6;
% c = 10;
% [ s, t ] = meshgrid( 0 : 0.3 : pi/2, 0 : 0.3 : pi );
% x = a * cos(s) .* cos( t );
% y = b * cos(s) .* sin( t );
% z = c * sin(s);
% % rotation
% ang = pi/6;
% xt = x * cos( ang ) - y * sin( ang );
% yt = x * sin( ang ) + y * cos( ang );
% % translation
shiftx = 0;
shifty = 0;
shiftz = 0;
% x = xt + shiftx;
% y = yt + shifty;
% z = z  + shiftz;
% 
% % add testing noise:
% noiseIntensity = 0.;
% x = x + randn( size( s ) ) * noiseIntensity;
% y = y + randn( size( s ) ) * noiseIntensity;
% z = z + randn( size( s ) ) * noiseIntensity;
% x = x(:);
% y = y(:);
% z = z(:);

Din=xlsread('124_Accel_Calibration_Log_2015_05_29_12_13_08.csv');

a = 0.9984;
b = 0.9984;
c = 0.9984;

for j=1:size(Din,1)
    theta=Din(j,11);
    phi=Din(j,12);
   Din(j,13)= -cosd( theta) * cosd( phi) * 0.9984;
   Din(j,14)= cosd( theta) * sind( phi) * 0.9984;
   Din(j,15)= sind( theta) * 0.9984;
end

X = [Din(:,13) Din(:,14) Din(:,15)];
x = Din(:,13);
y = Din(:,14);
z = Din(:,15);

% do the fitting
[ center, radii, evecs, v ] = ellipsoid_fit( [x y z ] );
fprintf( 'Ellipsoid center: %.3g %.3g %.3g\n', center );
fprintf( 'Ellipsoid radii : %.3g %.3g %.3g\n', radii );
fprintf( 'Ellipsoid evecs :\n' );
fprintf( '%.3g %.3g %.3g\n%.3g %.3g %.3g\n%.3g %.3g %.3g\n', ...
    evecs(1), evecs(2), evecs(3), evecs(4), evecs(5), evecs(6), evecs(7), evecs(8), evecs(9) );
fprintf( 'Algebraic form  :\n' );
fprintf( '%.3g ', v );
fprintf( '\n' );

% draw data
plot3( x, y, z, '.r' );
hold on;

%draw fit
maxd = max( [ a b c ] );
step = maxd / 50;
[ x, y, z ] = meshgrid( -maxd:step:maxd + shiftx, -maxd:step:maxd + shifty, -maxd:step:maxd + shiftz );

Ellipsoid = v(1) *x.*x +   v(2) * y.*y + v(3) * z.*z + ...
          2*v(4) *x.*y + 2*v(5)*x.*z + 2*v(6) * y.*z + ...
          2*v(7) *x    + 2*v(8)*y    + 2*v(9) * z;
p = patch( isosurface( x, y, z, Ellipsoid, 1 ) );
set( p, 'FaceColor', 'g', 'EdgeColor', 'none' );
view( -70, 40 );
axis vis3d;
camlight;
lighting phong;