close all
clear all
clc

filename = '124_Accel_Calibration_Log_2015_06_10_18_01_16_RT';
Din=xlsread(strcat(filename,'.csv'));

Din(find (Din(:,3)==0),:) = [];
D_out= zeros(size(Din,1),30);

load('W1.mat');
load('V1.mat');
load('T1.mat');
load('W2.mat');
load('V2.mat');
load('T2.mat');

raw_data1 = Din(:,3:5);
nf_inc_roll_test = max(Din(:,1));
nf_tf_roll_test = max(Din(:,2));
max_min_err = cell(1,nf_inc_roll_test);

cal_data1 = (W1*raw_data1'+repmat(V1,1,size(Din,1))+ (raw_data1'.^3) .* repmat([T1(1,1); T1(2,2); T1(3,3)],1,size(Din,1))  )';
cal_norm = sqrt(cal_data1(:,1).^2 + cal_data1(:,2).^2 +  cal_data1(:,3).^2);
cal_inc1 = (180/pi)*atan2(sqrt(cal_data1(:,1).^2 + cal_data1(:,2).^2), cal_data1(:,3));
TF1= -(180/pi) * atan2(cal_data1(:,2),-cal_data1(:,1));


for i=1:nf_inc_roll_test
    inc_at_tfs = cal_inc1(find(Din(:,1)==i));
    max_min_err{i} = (max(inc_at_tfs)-min(inc_at_tfs))/2;
end

raw_data2 = Din(:,6:8);
cal_data2 = (W2*raw_data2'+repmat(V2,1,size(Din,1))+ (raw_data2'.^3) .* repmat([T2(1,1); T2(2,2); T2(3,3)],1,size(Din,1))  )';
cal_inc2 = (180/pi)*atan2(sqrt(cal_data2(:,1).^2 + cal_data2(:,2).^2), cal_data2(:,3));

D_out(:,1:8) = Din(:,1:8);
D_out(:,11:13) = cal_data1;
D_out(:,14) = cal_norm;
D_out(:,16:18) = cal_data2;

D_out(:,22) = Din(:,10);
D_out(:,23) = cal_inc1;
D_out(:,24) = TF1;
D_out(:,29) = cal_inc1;
D_out(:,30) = cal_inc2;
D_out(:,31) =90 - Din(:,11);

xlswrite(strcat(filename,'_Added.csv'),D_out );







